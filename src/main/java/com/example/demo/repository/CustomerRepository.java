package com.example.demo.repository;

import com.example.demo.model.CustomerAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerAccount, Integer> {
    //    @Query("SELECT u FROM CustomerAcc u WHERE u.cardId = :cardId")
    //    CustomerAcc getCustomerAccByCardId(@Param("cardId") String cardId);
}
