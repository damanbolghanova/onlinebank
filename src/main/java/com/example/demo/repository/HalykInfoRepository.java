package com.example.demo.repository;

import com.example.demo.model.HalykTransactionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HalykInfoRepository extends JpaRepository<HalykTransactionInfo, Integer> {
}
