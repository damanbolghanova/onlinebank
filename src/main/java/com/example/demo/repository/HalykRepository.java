package com.example.demo.repository;

import com.example.demo.model.HalykBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HalykRepository extends JpaRepository<HalykBank, Integer> {
}
