package com.example.demo.exeption;

public class TransactionNotFoundException extends Exception{
    private long id;

    public TransactionNotFoundException(long id){
        super(String.format("Transaction with id : '%s' is not found", id));
    }
}
