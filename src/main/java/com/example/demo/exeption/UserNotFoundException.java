package com.example.demo.exeption;

public class UserNotFoundException extends Exception{
    private long id;

    public UserNotFoundException(long id){
        super(String.format("User with id '%s' is not found", id));
    }
}
