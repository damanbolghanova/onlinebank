package com.example.demo.controller;

import com.example.demo.model.*;
import com.example.demo.service.TransactionInfoService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class TransactionInfoController {
    @Autowired
    private TransactionInfoService transactionInfoService;

    @Autowired
    private UserService userService;

    @GetMapping("/info")
    public String viewHomePage(Model model, Principal principal){
        List<TransactionInfo> transactionInfoList = transactionInfoService.findTransactionList(principal.getName());
        User user = userService.findByUsername(principal.getName());
        CustomerAccount customerAccount = user.getCustomerAccount();
        model.addAttribute("TransactionList", transactionInfoList);
        model.addAttribute("customerAccount", customerAccount);
        return "main";
    }

    @GetMapping("/halykInfo")
    public String viewHome(Model model, Principal principal){
        List<HalykTransactionInfo> transactionInfoList = transactionInfoService.findHalykTransactionList(principal.getName());
        User user = userService.findByUsername(principal.getName());
        HalykBank halykBank = user.getHalykBank();
        model.addAttribute("HalykTransactionList", transactionInfoList);
        model.addAttribute("HalykBank", halykBank);
        return "mainHalyk";
    }

    @RequestMapping("mainHalyk")
    public String main(){
        return "mainHalyk";
    }
}
