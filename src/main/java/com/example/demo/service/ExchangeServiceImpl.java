package com.example.demo.service;

import com.example.demo.model.*;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.HalykInfoRepository;
import com.example.demo.repository.HalykRepository;
import com.example.demo.repository.TransactionInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;

@Service
public class ExchangeServiceImpl implements ExchangeService{
    @Autowired
    private UserService userService;
    @Autowired
    private TransactionInfoRepository transactionInfoRepository;
    @Autowired
    private HalykInfoRepository halykInfoRepository;
    @Autowired
    private HalykRepository halykRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<TransactionInfo> findTransactionList(String username) {
        User user = userService.findByUsername(username);
        List<TransactionInfo> transactionInfoList = user.getCustomerAccount().getTransactionInfos();
        return transactionInfoList;
    }

    @Override
    public List<HalykTransactionInfo> findHalykTransactionList(String username) {
        User user = userService.findByUsername(username);
        List<HalykTransactionInfo> transactionInfoList = user.getHalykBank().getHalykTransactionInfos();
        return transactionInfoList;
    }

    @Override
    public void betweenBanksTransfer(String from, String to, double amount, CustomerAccount customerAcc, HalykBank halykBank) throws Exception {
        if (from.equalsIgnoreCase("Kaspi") && to.equalsIgnoreCase("Halyk")){
            customerAcc.setBalance((int) (customerAcc.getBalance()-(amount+(amount*0.01))));
            halykBank.setBalance((int) (halykBank.getBalance()+amount));
            customerRepository.save(customerAcc);
            halykRepository.save(halykBank);

            Date date = new Date();
            TransactionInfo transactionInfo = new TransactionInfo(date, "Transfer from " + from + " to " + to, "Finished", (int)amount, customerAcc, customerAcc.getBalance(), "Kaspi");
            transactionInfoRepository.save(transactionInfo);
        }else if (from.equalsIgnoreCase("Halyk") && to.equalsIgnoreCase("Kaspi")){
            customerAcc.setBalance((int) (customerAcc.getBalance()+amount));
            halykBank.setBalance((int) (halykBank.getBalance()-(amount+(amount*0.01))));
            customerRepository.save(customerAcc);
            halykRepository.save(halykBank);

            Date date = new Date();
            HalykTransactionInfo transactionInfo = new HalykTransactionInfo(date, "Transfer from " + from + " to " + to, "Finished","Halyk", (int)amount, halykBank, halykBank.getBalance());
            halykInfoRepository.save(transactionInfo);
        }else{
            throw new Exception("Invalid Transfer");
        }
    }

    @Override
    public void saveCustomerDepositTransaction(TransactionInfo transactionInfo) {
        transactionInfoRepository.save(transactionInfo);
    }

    @Override
    public void saveHalykDepositTransaction(HalykTransactionInfo halykTransactionInfo) {
        halykInfoRepository.save(halykTransactionInfo);
    }

    @Override
    public void saveCustomerWithdrawTransaction(TransactionInfo transactionInfo) {
        transactionInfoRepository.save(transactionInfo);
    }

    @Override
    public void saveHalykWithdrawTransaction(HalykTransactionInfo halykTransactionInfo) {
        halykInfoRepository.save(halykTransactionInfo);
    }
}
