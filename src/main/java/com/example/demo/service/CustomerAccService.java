package com.example.demo.service;

import com.example.demo.model.CustomerAccount;

import java.security.Principal;
import java.util.List;

public interface CustomerAccService {
    List<CustomerAccount> getAllAccounts();
    CustomerAccount findCustomer(String username);
    void deposit(String bank, double amount, Principal principal);
    void withdraw(String bank, double amount, Principal principal);

}
