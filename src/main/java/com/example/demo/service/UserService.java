package com.example.demo.service;

import com.example.demo.model.Role;
import com.example.demo.model.User;

import java.util.Set;

public interface UserService {
    User findByUsername(String username);
    boolean checkUserExists(String username, String email);
    boolean checkUsernameExists(String username);
    boolean checkEmailExists(String email);
    User createUser(User user, Set<Role> userRoles);

}
