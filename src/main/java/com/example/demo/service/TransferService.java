package com.example.demo.service;

import com.example.demo.model.CustomerAccount;
import com.example.demo.model.TransactionInfo;

import java.util.List;

public interface TransferService {
    List<TransactionInfo> findTransactionInfo(String username);
    void transferBetweenCards(String from, String to, int amount, CustomerAccount customerAcc) throws Exception;

}
