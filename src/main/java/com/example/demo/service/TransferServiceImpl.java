package com.example.demo.service;

import com.example.demo.model.CustomerAccount;
import com.example.demo.model.TransactionInfo;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TransferServiceImpl implements TransferService{
    @Autowired
    UserService userService;

    @Override
    public List<TransactionInfo> findTransactionInfo(String username) {
        User user = userService.findByUsername(username);
        List<TransactionInfo> transactionInfoList = user.getCustomerAccount().getTransactionInfos();
        return transactionInfoList;
    }

    @Override
    public void transferBetweenCards(String from, String to, int amount, CustomerAccount customerAcc) throws Exception {

    }
}
