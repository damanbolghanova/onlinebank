package com.example.demo.service;

import com.example.demo.model.HalykTransactionInfo;
import com.example.demo.model.TransactionInfo;

import java.util.List;

public interface TransactionInfoService {
    List<TransactionInfo> getAllTransactionInfos();
    List<TransactionInfo> findTransactionList(String username);
    List<HalykTransactionInfo> findHalykTransactionList(String username);
    void saveDepositTransactionInfo(TransactionInfo transactionInfo);
    void saveHalykDepositInfo(HalykTransactionInfo halykTransactionInfo);
}
