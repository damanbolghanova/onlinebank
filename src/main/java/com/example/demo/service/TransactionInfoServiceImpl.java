package com.example.demo.service;

import com.example.demo.model.HalykTransactionInfo;
import com.example.demo.model.TransactionInfo;
import com.example.demo.model.User;
import com.example.demo.repository.HalykInfoRepository;
import com.example.demo.repository.TransactionInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionInfoServiceImpl implements TransactionInfoService{
    @Autowired
    private TransactionInfoRepository transactionInfoRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private HalykInfoRepository halykRepository;

    @Override
    public List<TransactionInfo> getAllTransactionInfos() {
        return transactionInfoRepository.findAll();
    }

    @Override
    public List<TransactionInfo> findTransactionList(String username) {
        User user = userService.findByUsername(username);
        List<TransactionInfo> transactionInfoList = user.getCustomerAccount().getTransactionInfos();
        return transactionInfoList;
    }

    @Override
    public List<HalykTransactionInfo> findHalykTransactionList(String username) {
        User user = userService.findByUsername(username);
        List<HalykTransactionInfo> halykTransactionInfos = user.getHalykBank().getHalykTransactionInfos();
        return halykTransactionInfos;
    }

    @Override
    public void saveDepositTransactionInfo(TransactionInfo transactionInfo) {
        transactionInfoRepository.save(transactionInfo);
    }

    @Override
    public void saveHalykDepositInfo(HalykTransactionInfo halykTransactionInfo) {
        halykRepository.save(halykTransactionInfo);
    }
}
