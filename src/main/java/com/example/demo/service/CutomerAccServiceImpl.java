package com.example.demo.service;

import com.example.demo.model.*;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.HalykRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@Service
public class CutomerAccServiceImpl implements CustomerAccService{
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private TransactionInfoService transactionInfoService;
    @Autowired
    private HalykRepository halykRepository;

    @Override
    public List<CustomerAccount> getAllAccounts() {
        return customerRepository.findAll();
    }

    @Override
    public CustomerAccount findCustomer(String username) {
        User user = userService.findByUsername(username);
        CustomerAccount customerAccount = user.getCustomerAccount();
        return customerAccount;
    }

    @Override
    public void deposit(String bank, double amount, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        if (bank.equalsIgnoreCase("kaspi")){
            CustomerAccount customerAccount = user.getCustomerAccount();
            if (amount>=100000){
                customerAccount.setBalance((int) (customerAccount.getBalance() + (amount-(amount*0.01))));
            }
            else{
                customerAccount.setBalance((int) (customerAccount.getBalance() + amount));
            }
            customerRepository.save(customerAccount);

            Date date = new Date();

            TransactionInfo transactionInfo = new TransactionInfo(date, "Deposit to KaspiBank account", "success", (int) amount, customerAccount, customerAccount.getBalance(), "Kaspi");
            transactionInfoService.saveDepositTransactionInfo(transactionInfo);
        }else if (bank.equalsIgnoreCase("Halyk")){
            HalykBank halykBank = user.getHalykBank();
            if (amount>=100000){
                halykBank.setBalance((int)(halykBank.getBalance() + (amount-(amount*0.01))));
            }else{
                halykBank.setBalance((int)(halykBank.getBalance() + amount));
            }

            halykRepository.save(halykBank);

            Date date = new Date();

            HalykTransactionInfo halykTransactionInfo = new HalykTransactionInfo(date, "Deposit to HalykBank account", "success","Halyk", (int) amount, halykBank, halykBank.getBalance());
            transactionInfoService.saveHalykDepositInfo(halykTransactionInfo);
        }
    }

    @Override
    public void withdraw(String bank, double amount, Principal principal) {
        if (bank.equalsIgnoreCase("Kaspi")) {
                User user = userService.findByUsername(principal.getName());
                CustomerAccount customerAcc = user.getCustomerAccount();
                customerAcc.setBalance((int) (customerAcc.getBalance() - amount - (amount*(1/100))));
                customerRepository.save(customerAcc);
                Date date = new Date();
                TransactionInfo transactionInfo = new TransactionInfo(date, "Withdraw from account", "success", (int) amount, customerAcc, customerAcc.getBalance(), "Kaspi");
                transactionInfoService.saveDepositTransactionInfo(transactionInfo);
        }
        else if(bank.equalsIgnoreCase(("Halyk"))) {
                User user = userService.findByUsername(principal.getName());
                HalykBank halykBank = user.getHalykBank();
                halykBank.setBalance((int) (halykBank.getBalance() - amount));
                halykRepository.save(halykBank);
                Date date = new Date();
                HalykTransactionInfo halykTransactionInfo = new HalykTransactionInfo(date, "Withdraw from HalykBank account", "success", "Halyk", (int)amount, halykBank, halykBank.getBalance());
                transactionInfoService.saveHalykDepositInfo(halykTransactionInfo);
        }
    }
}
