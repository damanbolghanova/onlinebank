package com.example.demo.service;

import com.example.demo.model.CustomerAccount;
import com.example.demo.model.HalykBank;
import com.example.demo.model.HalykTransactionInfo;
import com.example.demo.model.TransactionInfo;

import java.util.List;

public interface ExchangeService {
    List<TransactionInfo> findTransactionList(String username);
    List<HalykTransactionInfo> findHalykTransactionList(String username);

    void betweenBanksTransfer(String from, String to, double amount, CustomerAccount customerAcc, HalykBank halykBank) throws Exception;

    void saveCustomerDepositTransaction(TransactionInfo transactionInfo);
    void saveHalykDepositTransaction(HalykTransactionInfo halykTransactionInfo);
    void saveCustomerWithdrawTransaction(TransactionInfo transactionInfo);
    void saveHalykWithdrawTransaction(HalykTransactionInfo halykTransactionInfo);

}
